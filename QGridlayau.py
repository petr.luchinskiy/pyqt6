from PyQt6.QtCore import QSize
from PyQt6.QtWidgets import QApplication, QWidget, QPushButton, QMenu, QLineEdit, QHBoxLayout, QGridLayout
from PyQt6.QtGui import QIcon, QFont
import sys
import qdarktheme


# set window configuration
class Window(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowIcon(QIcon('images/python.png'))
        self.setGeometry(200, 200, 700, 400)
        self.setWindowTitle('QHboxLayout')

        grid = QGridLayout()


        btn1 = QPushButton('one')
        btn2 = QPushButton('two')
        btn3 = QPushButton('three')
        btn4 = QPushButton('four')
        btn5 = QPushButton('one')
        btn6 = QPushButton('two')
        btn7 = QPushButton('three')
        btn8 = QPushButton('four')
        btn9 = QPushButton('one')
        btn10 = QPushButton('two')
        btn11 = QPushButton('three')
        btn12  = QPushButton('four')
        grid.addWidget(btn1, 0,0)
        grid.addWidget(btn2, 0,1)
        grid.addWidget(btn3, 0,2)
        grid.addWidget(btn4, 0,3)
        grid.addWidget(btn5, 1,0)
        grid.addWidget(btn6, 1,1)
        grid.addWidget(btn7, 1,2)
        grid.addWidget(btn8, 1,3)
        grid.addWidget(btn9, 2,0)
        grid.addWidget(btn10, 2,1)
        grid.addWidget(btn11, 2,2)
        grid.addWidget(btn12, 2,3)
        self.setLayout(grid)






# show window
app = QApplication(sys.argv)
app.setStyleSheet(qdarktheme.load_stylesheet())
window = Window()

window.show()
sys.exit(app.exec())

