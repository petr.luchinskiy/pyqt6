from PyQt6.QtCore import QSize
from PyQt6.QtWidgets import QApplication, QWidget, QPushButton, QMenu, QLineEdit, QHBoxLayout
from PyQt6.QtGui import QIcon, QFont
import sys
import qdarktheme


# set window configuration
class Window(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowIcon(QIcon('images/python.png'))
        self.setGeometry(200, 200, 700, 400)
        self.setWindowTitle('QHboxLayout')

        hbox = QHBoxLayout()

        btn1 = QPushButton('one')
        btn2 = QPushButton('two')
        btn3 = QPushButton('three')
        btn4 = QPushButton('four')

        hbox.addWidget(btn1)
        hbox.addWidget(btn2)
        hbox.addWidget(btn3)
        hbox.addWidget(btn4)

        self.setLayout(hbox)






# show window
app = QApplication(sys.argv)
app.setStyleSheet(qdarktheme.load_stylesheet())
window = Window()

window.show()
sys.exit(app.exec())

