from PyQt6.QtCore import QSize
from PyQt6.QtWidgets import QApplication, QWidget, QPushButton, QMenu
from PyQt6.QtGui import QIcon, QFont
import sys
import qdarktheme


# set window configuration
class Window(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowIcon(QIcon('images/python.png'))
        self.setGeometry(200, 200, 700, 400)
        self.setWindowTitle('Python buttons')

        self.create_button()

    def create_button(self):
        btn = QPushButton('Click', self)
        btn.setGeometry(100, 100, 130, 130)
        btn.setFont(QFont('Times', 14, QFont.Weight.ExtraBold))
        btn.setIcon(QIcon('images/python.png'))
        btn.setIconSize(QSize(36, 36))

        #popup_menu
        menu=QMenu()
        menu.setFont(QFont('Times', 14))
        menu.setStyleSheet('background-color:red')
        menu.addAction("Copy")
        menu.addAction("Cut")
        menu.addAction("Paste")
        btn.setMenu(menu)



# show window
app = QApplication(sys.argv)
app.setStyleSheet(qdarktheme.load_stylesheet())
window = Window()

window.show()
sys.exit(app.exec())

