from PyQt6.QtCore import QSize
from PyQt6.QtWidgets import QApplication, QWidget, QPushButton, QMenu, QLineEdit
from PyQt6.QtGui import QIcon, QFont
import sys
import qdarktheme


# set window configuration
class Window(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowIcon(QIcon('images/python.png'))
        self.setGeometry(200, 200, 700, 400)
        self.setWindowTitle('Python buttons')

        line_edit = QLineEdit(self)
        line_edit.setFont(QFont('Times', 12))
        #line_edit.setText('Default text')
        line_edit.setPlaceholderText('Please enter your name')
       # line_edit.setEchoMode(QLineEdit.EchoMode.Password)



# show window
app = QApplication(sys.argv)
app.setStyleSheet(qdarktheme.load_stylesheet())
window = Window()

window.show()
sys.exit(app.exec())

