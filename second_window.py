import sys
from random import randint

from PyQt6.QtWidgets import (
    QApplication,
    QLabel,
    QMainWindow,
    QPushButton,
    QVBoxLayout,
    QWidget,
)

from PyQt6.QtWidgets import QApplication, QWidget, QLabel
from PyQt6.QtGui import QIcon, QFont
import sys


class AnotherWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowIcon(QIcon('images/python.png'))
        self.setGeometry(200, 200, 700, 400)
        self.setWindowTitle('Python GUI Development')

        label = QLabel('Python Label ', self)
        label.setText('new text is here')
        label.move(100, 100)
        label.setFont(QFont('Sanserif', 15))
        label.setStyleSheet('color: red')
        label.setText(str(12))
        label.setNum(12)
        label.clear()


# show window
app = QApplication(sys.argv)
window = AnotherWindow()
window.show()
sys.exit(app.exec())
