from PyQt6.QtCore import QSize
from PyQt6.QtWidgets import QApplication, QWidget, QPushButton, QMenu, QLineEdit, QHBoxLayout, QGridLayout, QLabel
from PyQt6.QtGui import QIcon, QFont
import sys
import qdarktheme


# set window configuration
class Window(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowIcon(QIcon('images/python.png'))
        self.setGeometry(200, 200, 700, 400)
        self.setWindowTitle('QHboxLayout')

        self.create_widget()

    def create_widget(self):
        hbox = QHBoxLayout()
        btn = QPushButton('change text')
        btn.clicked.connect(self.clicked_btn)
        self.label = QLabel('default text')

        hbox.addWidget(btn)
        hbox.addWidget(self.label)

        self.setLayout(hbox)

    def clicked_btn(self):
        self.label.setText('Another text')
        self.label.setFont(QFont('Times', 15))
        self.label.setStyleSheet('color:red')
# show window
app = QApplication(sys.argv)
app.setStyleSheet(qdarktheme.load_stylesheet())
window = Window()

window.show()
sys.exit(app.exec())

